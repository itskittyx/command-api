plugins {
    kotlin("jvm") version "1.9.0"
}

group = "com.gitlab.kittyx."
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {

    implementation("com.fasterxml.jackson.core:jackson-annotations:2.15.2")
}