# Command API

### Данная библиотека предназначена для упрощения создания команд

### Подключение библиотеки в проект:
#### Gradle Groovy DSL:
``` groovy
repositories {
    mavenCentral()
    maven { url 'https://jitpack.io' }
}

dependencies {
    implementation 'com.gitlab.kittyx:command-api:1.0.0'
}
```

#### Gradle Kotlin DSL:
``` groovy
repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    implementation("com.gitlab.kittyx:command-api:1.0.0")
}
```

#### Maven:
``` xml
<repository>
  <id>jitpack.io</id>
  <url>https://jitpack.io</url>
</repository>

<dependency>
  <groupId>com.gitlab.kittyx</groupId>
  <artifactId>command-api</artifactId>
  <version>1.0.1</version>
</dependency>
```
