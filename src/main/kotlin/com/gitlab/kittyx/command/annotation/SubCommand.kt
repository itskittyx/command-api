package com.gitlab.kittyx.command.annotation

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class SubCommand(
    /**
     * Названия подкоманды
     */
    vararg val commandNames: String
)