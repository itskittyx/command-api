package com.gitlab.kittyx.command.annotation

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class Default(

    /**
     * Вызывать ли метод помеченный данной аннотацией при ненайденной подкоманды
     */
    val subCommandNotFoundInvoke: Boolean = false
)