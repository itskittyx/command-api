package com.gitlab.kittyx.command.annotation

/**
 * Аннотация для указания прав для использования команды
 *
 * * Kotin: ("*", "default", "user")
 * * Java: ({"*", "default", "user"}) {} - ставятся автоматически благодаря IDEA
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class Permission(

    vararg val value: String = []

)
