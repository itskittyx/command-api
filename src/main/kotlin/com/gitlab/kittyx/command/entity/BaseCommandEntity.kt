package com.gitlab.kittyx.command.entity

import com.gitlab.kittyx.command.entity.data.CommandContentData
import java.lang.reflect.Method

sealed class BaseCommandEntity(
    open val method: Method,
    open val commandContent: CommandContentData
) {


}