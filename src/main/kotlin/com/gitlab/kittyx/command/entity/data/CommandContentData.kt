package com.gitlab.kittyx.command.entity.data

import com.gitlab.kittyx.command.BaseCommand
import com.gitlab.kittyx.command.annotation.Cooldown
import com.gitlab.kittyx.command.annotation.MinArg
import com.gitlab.kittyx.command.annotation.Permission
import java.lang.reflect.Method

class CommandContentData {
    var cooldown: Cooldown? = null
    var minArg: MinArg? = null
    var permission: Permission? = null

    constructor(command: BaseCommand) {
        command.javaClass.apply {
            cooldown = if (this.isAnnotationPresent(Cooldown::class.java)) this.getAnnotation(Cooldown::class.java) else null
            minArg = if (this.isAnnotationPresent(MinArg::class.java)) this.getAnnotation(MinArg::class.java) else null
            permission = if (this.isAnnotationPresent(Permission::class.java)) this.getAnnotation(Permission::class.java) else null
        }
    }

    constructor(method: Method) {
        cooldown = if (method.isAnnotationPresent(Cooldown::class.java)) method.getAnnotation(Cooldown::class.java) else null
        minArg = if (method.isAnnotationPresent(MinArg::class.java)) method.getAnnotation(MinArg::class.java) else null
        permission = if (method.isAnnotationPresent(Permission::class.java)) method.getAnnotation(Permission::class.java) else null
    }
}